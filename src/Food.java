import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Food {
    private JPanel root;
    private JButton Button1;
    private JButton Button2;
    private JButton Button3;
    private JTextPane textPane1;
    private JButton Button4;
    private JButton Button5;
    private JButton Button6;


    private JButton checkOutButton;
    private JTextPane textPane2;

    //クラスを使ってまとめて表示する
    void order(String food){

        int confirmation=JOptionPane.showConfirmDialog(
                null,
                food+"食べたいの？",
                "オーダーを確認するでやんす",
                JOptionPane.YES_NO_OPTION
        );
        if(confirmation==0){
            textPane1.setText("得意料理だから期待してな");

            JOptionPane.showMessageDialog(null,"おっけー、"+food+"作るね");

        }
    }

    public Food() {

        Button1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Button1.setIcon(new ImageIcon(
                        this.getClass().getResource("")
                ));
                order("天ぷら");
            }

        });
        Button2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                order("ラーメン");
            }
        });
        Button3.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("うどん");
            }
        });
        Button4.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

            }
        });
        Button5.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

            }
        });
        Button6.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

            }
        });
        checkOutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

            }
        });
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("Food");
        frame.setContentPane(new Food().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

}


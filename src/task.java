import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class task {
    private JPanel root;
    private JLabel topLabel;
    private JButton tempuraButton;
    private JButton takoyakiButton;
    private JButton udonButton;
    private JButton curryRiceButton;
    private JButton friedChickenButton;
    private JButton iceCreamButton;
    private JLabel total;
    private JButton checkOutButton;
    private JTextPane orderedItemTextPane1;

    int sum=0;

    void order(String food, int Price) {
        int confirmation = JOptionPane.showConfirmDialog(null,
                "Would you like to order "+food+"?",
                "Order Confirmation",
                JOptionPane.YES_NO_OPTION);
        if (confirmation == 0) {
            JOptionPane.showMessageDialog(null, "Thank you for ordering" + food + "!.It will be serving as soon as possible.");
            String currentText = orderedItemTextPane1.getText();
            orderedItemTextPane1.setText(currentText + food + Price + "yen" + "\n");
            sum += Price;
            total.setText("Total:" + sum + "yen");
        }

    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("button");
        frame.setContentPane(new task().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

    public task() {

        tempuraButton.setIcon(new ImageIcon( this.getClass().getResource("Tempura.jpg")));
        takoyakiButton.setIcon(new ImageIcon( this.getClass().getResource("Takoyaki.jpg")));
        udonButton.setIcon(new ImageIcon( this.getClass().getResource("Udon.jpg")));
        curryRiceButton.setIcon(new ImageIcon( this.getClass().getResource("Curryrice.jpg")));
        friedChickenButton.setIcon(new ImageIcon( this.getClass().getResource("Friedchicken.jpg")));
        iceCreamButton.setIcon(new ImageIcon( this.getClass().getResource("Icecream.jpg")));
        tempuraButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Tempura", 500);
            }
        });

        takoyakiButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Takoyaki", 305);
            }
        });

        udonButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Udon", 330);
            }
        });
        curryRiceButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Curry rice", 645);
            }
        });
        friedChickenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Fried chicken", 440);
            }
        });
        iceCreamButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Ice cream", 100);
            }
        });
        checkOutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation = JOptionPane.showConfirmDialog(null,
                        "Would you like to order Checkout?",
                        "Checkout Confirmation",
                        JOptionPane.YES_NO_OPTION);
                if (confirmation == 0) {
                    orderedItemTextPane1.setText(null);
                    JOptionPane.showMessageDialog(null, "Thank you. the total price is " + sum + "yen.");
                    sum = 0;
                    total.setText("Total:" + sum + "yen");
                }
            }
        });
    }



}
